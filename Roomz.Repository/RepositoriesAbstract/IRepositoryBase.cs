﻿using Roomz.Repository.Extensions;
using System.Linq.Expressions;
using System.Linq;
using System;

namespace Roomz.Repository.RepositoriesAbstract
{
    public interface IRepositoryBase<T>
    {
        IQueryable<T> GetEntities(Expression<Func<T, bool>> predicate, Func<IIncludable<T>, IIncludable> includeProperties = null, bool trackChanges = false);

        void CreateEntity(T entity);

        void UpdateEntity(T entity);

        void DeleteEntity(T entity);
        void DeleteRange(IQueryable<T> entities);
    }
}
