﻿using Roomz.Repository.Extensions;
using Roomz.Domain.Entities;
using Roomz.Domain.Filters;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System;

namespace Roomz.Repository.RepositoriesAbstract.EntitySpecific
{
    public interface IInputRepository
    {
        Task<IEnumerable<Input>> Get(Expression<Func<Input, bool>> predicate, Func<IIncludable<Input>, IIncludable> includeProperties = null, bool trackChanges = false, int count = 0);
        Task<IEnumerable<Input>> GetWithFilters(InputFilter filter, Expression<Func<Input, bool>> predicate = null, bool trackChanges = false);

        Task<Input> GetById(Guid id, bool trackChanges = false);

        Task<int> CountAsync(Expression<Func<Input, bool>> predicate);

        void Create(Input entity);
        void Delete(Input entity);
    }
}
