﻿using Roomz.Repository.RepositoriesAbstract.EntitySpecific;
using System.Threading.Tasks;

namespace Roomz.Repository.RepositoriesAbstract
{
    public interface IRepositoryManager
    {
        IInputRepository Inputs { get; }

        Task SaveAsync();
    }
}
