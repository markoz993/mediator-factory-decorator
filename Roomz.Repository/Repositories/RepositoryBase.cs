﻿using Roomz.Repository.RepositoriesAbstract;
using Roomz.Repository.Extensions;
using Roomz.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Linq;
using System;

namespace Roomz.Repository.Repositories
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected RepositoryContext RepositoryContext;

        protected RepositoryBase(RepositoryContext repositoryContext)
        {
            RepositoryContext = repositoryContext;
        }

        public IQueryable<T> GetEntities(Expression<Func<T, bool>> predicate, Func<IIncludable<T>, IIncludable> includeProperties = null, bool trackChanges = false) =>
            trackChanges ?
                RepositoryContext.Set<T>().Where(predicate).IncludeProperties(includeProperties) :
                RepositoryContext.Set<T>().Where(predicate).IncludeProperties(includeProperties).AsNoTracking();

        public void CreateEntity(T entity) =>
            RepositoryContext.Set<T>().Add(entity);

        public void UpdateEntity(T entity) =>
            RepositoryContext.Set<T>().Update(entity);

        public void DeleteEntity(T entity) =>
            RepositoryContext.Set<T>().Remove(entity);

        public void DeleteRange(IQueryable<T> entities) =>
            RepositoryContext.Set<T>().RemoveRange(entities);
    }
}
