﻿using Roomz.Repository.RepositoriesAbstract.EntitySpecific;
using Roomz.Repository.Repositories.EntitySpecific;
using Roomz.Repository.RepositoriesAbstract;
using Roomz.Domain;
using System.Threading.Tasks;

namespace Roomz.Repository.Repositories
{
    public class RepositoryManager : IRepositoryManager
    {
        private RepositoryContext _repositoryContext;

        private IInputRepository _inputRepository;

        public RepositoryManager(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        public IInputRepository Inputs
        {
            get
            {
                if (_inputRepository == null)
                    _inputRepository = new InputRepository(_repositoryContext);

                return _inputRepository;
            }
        }

        public Task SaveAsync() => _repositoryContext.SaveChangesAsync();
    }
}
