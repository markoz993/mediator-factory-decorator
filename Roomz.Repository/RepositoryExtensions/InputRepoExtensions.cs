﻿using Roomz.Repository.Extensions;
using Roomz.Domain.Entities;
using System.Linq.Dynamic.Core;
using System.Linq;

namespace Roomz.Repository.RepositoryExtensions
{
    public static class InputRepoExtensions
    {
        public static IQueryable<Input> Search(this IQueryable<Input> data, string searchTerm)
        {
            if (string.IsNullOrWhiteSpace(searchTerm))
                return data;

            var lowerCaseTerm = searchTerm.Trim().ToLower();

            return data.Where(e =>
                e.Id.ToString().ToLower().Contains(lowerCaseTerm) ||
                e.OptimizationAlgorithm.ToLower().Contains(lowerCaseTerm));
        }

        public static IQueryable<Input> Sort(this IQueryable<Input> data, string orderByQueryString)
        {
            if (string.IsNullOrWhiteSpace(orderByQueryString))
                return data.OrderBy(e => e.TimeCreated);

            var orderQuery = OrderQueryBuilder.CreateOrderQuery<Input>(orderByQueryString);

            return string.IsNullOrWhiteSpace(orderQuery)
                ? data.OrderBy(e => e.TimeCreated)
                : data.OrderBy(orderQuery);
        }
    }
}
