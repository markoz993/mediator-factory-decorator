﻿using System.Collections.Generic;
using System;

namespace Roomz.Domain.DTOs
{
    public class InputDto
    {
        public Guid Id { get; set; }

        public string OptimizationAlgorithm { get; set; }

        public int MaxWeight { get; set; }

        public int CalculatedWeight { get; set; }

        public IEnumerable<WeightDto> PickedWeights { get; set; }

        public DateTime TimeCreated { get; set; }
    }
}
