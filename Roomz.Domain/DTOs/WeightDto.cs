﻿using System;

namespace Roomz.Domain.DTOs
{
    public class WeightDto
    {
        public Guid Id { get; set; }

        public InputDto Input { get; set; }
        public Guid InputId { get; set; }

        public string Name { get; set; }
        public int Value { get; set; }
    }
}
