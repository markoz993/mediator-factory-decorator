﻿using Roomz.Domain.Extensions;
using Roomz.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Roomz.Domain
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options)
           : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyModelConfigurations();
        }

        public DbSet<Input> Inputs { get; set; }
        public DbSet<Weight> Weights { get; set; }
    }
}