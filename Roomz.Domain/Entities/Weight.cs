﻿using System;

namespace Roomz.Domain.Entities
{
    public class Weight
    {
        public Guid Id { get; set; }

        public Input Input { get; set; }
        public Guid InputId { get; set; }

        public string Name { get; set; }
        public int Value { get; set; }
    }
}