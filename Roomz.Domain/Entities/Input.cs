﻿using System.Collections.Generic;
using System;

namespace Roomz.Domain.Entities
{
    public class Input
    {
        public Guid Id { get; set; }

        public string OptimizationAlgorithm { get; set; }

        public int MaxWeight { get; set; }

        public int CalculatedWeight { get; set; }

        public ICollection<Weight> PickedWeights { get; set; }

        public DateTime TimeCreated { get; set; }
    }
}
