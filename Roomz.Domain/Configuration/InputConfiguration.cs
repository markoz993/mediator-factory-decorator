﻿using Roomz.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace Roomz.Domain.Configuration
{
    public class InputConfiguration : IEntityTypeConfiguration<Input>
    {
        public void Configure(EntityTypeBuilder<Input> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.OptimizationAlgorithm).HasMaxLength(256).IsRequired();
            builder.Property(x => x.CalculatedWeight).IsRequired();
            builder.Property(x => x.MaxWeight).IsRequired();

            builder.HasMany<Weight>(x => x.PickedWeights).WithOne(x => x.Input).HasForeignKey(x => x.InputId);
        }
    }
}