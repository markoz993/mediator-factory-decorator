﻿using Roomz.Domain.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Roomz.Domain.Extensions
{
    public static class ModelBuilderModelExtensions
    {
        public static void ApplyModelConfigurations(this ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new InputConfiguration());
            modelBuilder.ApplyConfiguration(new WeightConfiguration());
        }
    }
}
