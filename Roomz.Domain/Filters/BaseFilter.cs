﻿namespace Roomz.Domain.Filters
{
    public class BaseFilter
    {
        private const int MAX_PAGE_SIZE = 200;
        private int _pageSize = 10;

        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > MAX_PAGE_SIZE) ? MAX_PAGE_SIZE : value;
        }

        public int PageNumber { get; set; } = 1;

        public string OrderBy { get; set; }

        public int Take { get; set; }
        public int Skip { get; set; }
    }
}
