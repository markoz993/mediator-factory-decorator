﻿namespace Roomz.Domain.Filters
{
    public class InputFilter : BaseFilter
    {
        public InputFilter()
        {
            OrderBy = "TimeCreated";
        }

        public string SearchTerm { get; set; }
    }
}
