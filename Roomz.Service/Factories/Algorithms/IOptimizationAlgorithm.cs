﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Roomz.Service.Factories.Algorithms
{
    public interface IOptimizationAlgorithm
    {
        Task<IEnumerable<(string, int)>> Optimize(IEnumerable<(string, int)> values, string algorithm, int max);
    }
}