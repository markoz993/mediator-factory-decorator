﻿using System.Collections.Immutable;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Roomz.Service.Factories.Algorithms
{
    public class KnapsackOptimizationAlgorithm : IOptimizationAlgorithm
    {
        public async Task<IEnumerable<(string, int)>> Optimize(IEnumerable<(string, int)> values, string algorithm, int max)
        {
            ImmutableList<ImmutableStack<(string nodeName, int totalWeight)>> Visitor(ImmutableStack<(string nodeName, int weight)> stack, LinkedListNode<(string, int)> node)
            {
                if (node.Next == null)
                {
                    return ImmutableList.Create(stack);
                }
                var (nodeName, weight) = node.Value;
                var (_, totalWeight) = stack.Peek();
                if (totalWeight + weight > max)
                {
                    return Visitor(stack, node.Next);
                }
                else
                {
                    return Visitor(stack.Push((nodeName, totalWeight + weight)), node.Next).AddRange(Visitor(stack, node.Next));
                }
            };

            var nodes = new LinkedList<(string, int)>(values);
            var result = await Task.Run(() => Visitor(ImmutableStack.Create(("InitialState ", 0)), nodes.First).ToArray());
            var bestStack = result.OrderByDescending(x => x.Peek().totalWeight).First();

            return bestStack.Join(nodes, x => x.nodeName, x => x.Item1, (outer, inner) => inner);
        }
    }
}