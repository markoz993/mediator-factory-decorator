﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Roomz.Service.Factories.Algorithms
{
    public class GreedyOptimizationAlgorithm : IOptimizationAlgorithm
    {
        public async Task<IEnumerable<(string, int)>> Optimize(IEnumerable<(string, int)> values, string algorithm, int max)
        {
            var total = 0;
            var result = new List<(string, int)>();

            foreach (var value in values.OrderByDescending(x => x.Item2))
            {
                var (name, weight) = value;
                if (total + weight > max)
                    continue;

                total += weight;

                await Task.Run(() => result.Add(value));
            }

            return result;
        }
    }
}
