﻿using Roomz.Service.Factories.Algorithms;

namespace Roomz.Service.Factories
{
    public interface IOptimizationAlgorithmsFactory
    {
        IOptimizationAlgorithm CreateOptimizationAlgorithm(string algorithm);
    }
}
