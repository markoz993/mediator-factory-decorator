﻿using Roomz.Service.Factories.Algorithms;
using System;

namespace Roomz.Service.Factories
{
    public class OptimizationAlgorithmsFactory : IOptimizationAlgorithmsFactory
    {
        public IOptimizationAlgorithm CreateOptimizationAlgorithm(string algorithm)
        {
            switch (algorithm.ToLower())
            {
                case "greedy": return new GreedyOptimizationAlgorithm();
                case "knapsack": return new KnapsackOptimizationAlgorithm();
                default: throw new InvalidOperationException();
            }
        }
    }
}
