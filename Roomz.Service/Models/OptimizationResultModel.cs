﻿using Roomz.Domain.DTOs;
using System.Collections.Generic;
using System;

namespace Roomz.Service.Models
{
    public class OptimizationResultModel
    {
        public string OptimizationAlgorithm { get; set; }

        public int MaxWeight { get; set; }
        public int CalculatedWeight { get; set; }

        public IEnumerable<WeightDto> PickedWeights { get; set; }

        public override string ToString()
        {
            var result = $"Optimization agorithm used: {OptimizationAlgorithm}{Environment.NewLine}Maximum weight: {MaxWeight}{Environment.NewLine}Calculated weight: {CalculatedWeight}{Environment.NewLine}Weight that were picked:";
            foreach (var weight in PickedWeights)
                result += $"{weight.Name}: {weight.Value}{Environment.NewLine}";

            return result;
        }
    }
}
