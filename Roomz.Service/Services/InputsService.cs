﻿using Roomz.Repository.RepositoriesAbstract;
using Roomz.Service.ServicesAbstract;
using Roomz.Service.Factories;
using Roomz.Service.Options;
using Roomz.Domain.Entities;
using Roomz.Service.Helpers;
using Roomz.Service.Models;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;
using AutoMapper;

namespace Roomz.Service.Services
{
    public class InputsService : IInputsService
    {
        private readonly ConfigurationOptions _configuration;
        private readonly IRepositoryManager _manager;
        private readonly IOptimizationAlgorithmsFactory _factory;
        private readonly IRandomGeneratorHelper _generator;
        private readonly IMapper _mapper;

        public InputsService(
            IOptions<ConfigurationOptions> configuration, 
            IRepositoryManager manager, 
            IOptimizationAlgorithmsFactory factory,
            IRandomGeneratorHelper generator,
            IMapper mapper)
        {
            _configuration = configuration.Value;
            _manager = manager;
            _factory = factory;
            _generator = generator;
            _mapper = mapper;
        }

        public async Task<string> OptimizeUsingAlgorithm(string algorithm)
        {
            var randomEntities = await _generator.GenerateRandomResults();
            var optimization = _factory.CreateOptimizationAlgorithm(algorithm);

            var results = await optimization.Optimize(randomEntities, algorithm, _configuration.MaxWeight);

            return await CreateEntities(results, algorithm);
        }

        private async Task<string> CreateEntities(IEnumerable<(string, int)> results, string algorithm)
        {
            var input = new Input
            {
                CalculatedWeight = results.Sum(x => x.Item2),
                MaxWeight = _configuration.MaxWeight,
                OptimizationAlgorithm = algorithm,
                PickedWeights = FillWeights(results),
                TimeCreated = DateTime.Now
            };

            try
            {
                _manager.Inputs.Create(input);
                await _manager.SaveAsync();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Entities were not created successfully.", ex.Message);
            }

            return (_mapper.Map<OptimizationResultModel>(input)).ToString();
        }

        private ICollection<Weight> FillWeights(IEnumerable<(string, int)> results) =>
            results.Select(x => new Weight { Name = x.Item1, Value = x.Item2 }).ToList();
    }
}
