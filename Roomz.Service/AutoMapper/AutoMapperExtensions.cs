﻿using AutoMapper;

namespace Roomz.Service.AutoMapper
{
    public static class AutoMapperExtensions
    {
        public static TResult MapTo<TResult>(this object self, IMapper mapper)
        {
            if (self == null)
            {
                return default;
            }

            return (TResult)mapper.Map(self, self.GetType(), typeof(TResult));
        }
    }
}
