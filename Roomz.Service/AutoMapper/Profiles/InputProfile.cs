﻿using Roomz.Domain.Entities;
using Roomz.Service.Models;
using Roomz.Domain.DTOs;
using AutoMapper;

namespace Roomz.Service.AutoMapper.Profiles
{
    public class InputProfile : Profile
    {
        public InputProfile()
        {
            CreateMap<Input, InputDto>().ReverseMap();

            CreateMap<Input, OptimizationResultModel>();
        }
    }
}
