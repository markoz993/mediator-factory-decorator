﻿using Roomz.Domain.Entities;
using Roomz.Domain.DTOs;
using AutoMapper;

namespace Roomz.Service.AutoMapper.Profiles
{
    public class WeightProfile : Profile
    {
        public WeightProfile()
        {
            CreateMap<Weight, WeightDto>().ReverseMap();
        }
    }
}
