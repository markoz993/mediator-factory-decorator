﻿using Roomz.Service.AutoMapper.Profiles;
using AutoMapper;

namespace Roomz.Service.AutoMapper
{
    public class AutoMapperConfiguration
    {
        public static IMapper Configure()
        {
            var configuration = CreateConfiguration();
            return configuration.CreateMapper();
        }

        private static MapperConfiguration CreateConfiguration()
        {
            var configuration = new MapperConfiguration(config =>
            {
                GeneralConfiguration(config);
                config.AddProfile<InputProfile>();
                config.AddProfile<WeightProfile>();
            });

            return configuration;
        }

        private static void GeneralConfiguration(IMapperConfigurationExpression cfg)
        {
            cfg.AllowNullCollections = true;
            cfg.ShouldMapProperty = p => p.GetMethod != null && p.GetMethod.IsPublic && p.Name != "ExtensionData";
        }
    }
}
