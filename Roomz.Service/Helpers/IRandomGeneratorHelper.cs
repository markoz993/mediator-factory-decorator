﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Roomz.Service.Helpers
{
    public interface IRandomGeneratorHelper
    {
        Task<IEnumerable<(string, int)>> GenerateRandomResults();
    }
}
