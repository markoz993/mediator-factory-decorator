﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace Roomz.Service.Helpers
{
    public class RandomGeneratorHelper : IRandomGeneratorHelper
    {
        public async Task<IEnumerable<(string, int)>> GenerateRandomResults() =>
            await Task.Run(() =>
            {
                Task.Delay(5000).GetAwaiter().GetResult();

                var random = new Random(new Random().Next());
                var objects = Enumerable.Range(0, 20)
                    .Select(x => ($"Object{x}", random.Next(5, 20)))
                    .Where(x => x.Item2 != 0)
                    .ToArray();

                return objects;
            });
    }
}
