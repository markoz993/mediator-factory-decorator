﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace Roomz.Service.Helpers
{
    public class RandomGeneratorLoggingDecorator : IRandomGeneratorHelper
    {
        private readonly int _maxWeight;
        private readonly IRandomGeneratorHelper _innerService;

        public RandomGeneratorLoggingDecorator(int maxWeight, IRandomGeneratorHelper innerService)
        {
            _maxWeight = maxWeight;
            _innerService = innerService;
        }

        public async Task<IEnumerable<(string, int)>> GenerateRandomResults()
        {
            Console.WriteLine("Generating random numbers ...");
         
            var results = await _innerService.GenerateRandomResults();

            Console.WriteLine("+-------------+--------------+");
            Console.WriteLine("+     Name         Weight    +");
            Console.WriteLine("+-------------+--------------+");

            foreach (var @object in results)
            {
                var (objectName, objectWeight) = @object;
                Console.WriteLine(string.Format("+ {0,8} + {1,2} +", objectName, objectWeight));
            }

            Console.WriteLine("+-------------+--------------+");
            Console.WriteLine();
            Console.WriteLine("The following combination doesn't exceed the weight: " + _maxWeight);

            return results;
        }
    }
}
