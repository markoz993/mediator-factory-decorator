﻿using System.Threading.Tasks;

namespace Roomz.Service.ServicesAbstract
{
    public interface IInputsService
    {
        Task<string> OptimizeUsingAlgorithm(string algorithm);
    }
}
