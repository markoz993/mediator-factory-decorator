﻿using Roomz.Service.Models;
using MediatR;

namespace Roomz.Application.Commands
{
    public class GetOptimizationAlgorithmRequest : IRequest<string>
    {
        public string Algorithm { get; set; }
    }
}
