﻿using Roomz.Service.ServicesAbstract;
using System.Threading.Tasks;
using System.Threading;
using MediatR;

namespace Roomz.Application.Commands
{
    public class GetOptimizationAlgorithmHandler : IRequestHandler<GetOptimizationAlgorithmRequest, string>
    {
        private readonly IInputsService _service;

        public GetOptimizationAlgorithmHandler(IInputsService service)
        {
            _service = service;
        }

        public async Task<string> Handle(GetOptimizationAlgorithmRequest request, CancellationToken cancellationToken) =>
            await _service.OptimizeUsingAlgorithm(request.Algorithm);
    }
}
