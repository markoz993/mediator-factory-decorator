﻿using System;

namespace Roomz.Api.Util
{
    public class RequestParamValidator
    {
        public Func<bool> Rule { get; }
        public string Message { get; }

        public RequestParamValidator(Func<bool> rule, string message)
        {
            Rule = rule;
            Message = message;
        }

        public static RequestParamValidator AlgorithmValidator(string alg) =>
            new RequestParamValidator(IdInvalidRule(alg), "Algorithm type received is not valid.");

        private static Func<bool> IdInvalidRule(string algorithm) =>
            () => string.IsNullOrWhiteSpace(algorithm);
    }
}