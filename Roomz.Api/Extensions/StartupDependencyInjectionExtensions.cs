﻿using Roomz.Repository.RepositoriesAbstract.EntitySpecific;
using Roomz.Repository.Repositories.EntitySpecific;
using Roomz.Service.ServicesAbstract;
using Roomz.Service.Factories;
using Roomz.Service.Services;
using Roomz.Service.Options;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace Roomz.Api.Extensions
{
    public static class StartupDependencyInjectionExtensions
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IInputRepository, InputRepository>();
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddTransient<IInputsService, InputsService>();
            services.AddTransient<IOptimizationAlgorithmsFactory, OptimizationAlgorithmsFactory>();
        }

        public static void AddOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions<DbOptions>(configuration, DbOptions.CONNECTION_STRINGS);
            services.AddOptions<ConfigurationOptions>(configuration, ConfigurationOptions.CONFIGURADION_VARIABLES);
            
        }

        public static void AddOptions<TOptions>(this IServiceCollection services, IConfiguration configuration, string sectionName) where TOptions : class
        {
            services.Configure<TOptions>(configuration.GetSection(sectionName));
        }
    }
}