﻿using Roomz.Application.Commands;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using MediatR;

namespace Roomz.Api.Controllers.api
{
    [Route("api/my-function")]
    [ApiController]
    public class InputsController : BaseApiController
    {
        public InputsController(IMediator mediator) : base(mediator) 
        { }

        [HttpGet("")]
        public async Task<IActionResult> Index([FromQuery] string algorithm) =>
            await ProcessRequest(request: () => Mediator.Send(new GetOptimizationAlgorithmRequest { Algorithm = algorithm }));
    }
}
