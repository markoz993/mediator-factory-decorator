﻿using Roomz.Api.Logging;
using Roomz.Api.Util;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Linq;
using System;
using MediatR;

namespace Roomz.Api.Controllers
{
    public class BaseApiController : Controller
    {
        private IMediator? _mediator;
        private ILogger? _logger;

        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
        protected ILogger Logger => _logger ??= Logging.Logger.Create(@"D:\logs.txt");

        public BaseApiController(IMediator mediator)
        {
            _mediator = mediator;
        }

        protected async Task<JsonResult> ProcessRequest<T>(Func<Task<T>> request, params RequestParamValidator[] paramValidators)
        {
            var invalidParams = paramValidators?.Where(pv => pv.Rule());
            if (invalidParams.Any())
                return Json(string.Join(". ", invalidParams.Select(s => s.Message)));

            try
            {
                var response = await request();

                if (response == null)
                    return Json("No response content was received.");

                return Json(response);
            }
            catch (Exception e)
            {
                Logger.LogMessage(e.ToString());
                return Json("Internal server error has occured.");
            }
        }
    }
}