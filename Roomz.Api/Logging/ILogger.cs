﻿namespace Roomz.Api.Logging
{
    public interface ILogger
    {
        void LogMessage(string message);
    }
}
