using Roomz.Application.Commands;
using Roomz.Service.AutoMapper;
using Roomz.Service.Helpers;
using Roomz.Service.Options;
using Roomz.Service.Models;
using Roomz.Api.Extensions;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System;
using MediatR;

namespace Roomz.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());

            var mapper = AutoMapperConfiguration.Configure();
            services.AddSingleton(mapper);

            //Cookies
            services.Configure<CookiePolicyOptions>(options => { options.MinimumSameSitePolicy = SameSiteMode.None; });

            services.ConfigureCors();
            services.ConfigureServerIntegration();
            services.ConfigureSqlContext(Configuration);
            services.ConfigureRepositoryManager();

            services.AddRepositories();
            services.AddServices();
            services.AddOptions(Configuration);

            services.AddOptions<ConfigurationOptions>(Configuration, ConfigurationOptions.CONFIGURADION_VARIABLES);

            services.AddScoped<IRandomGeneratorHelper>(serviceProvider => new RandomGeneratorHelper());
            services.Decorate<IRandomGeneratorHelper>((inner, provider) => new RandomGeneratorLoggingDecorator(Configuration.GetValue<int>("ConfigurationVariables"), inner));

            services.AddTransient<IRequestHandler<GetOptimizationAlgorithmRequest, string>, GetOptimizationAlgorithmHandler>();

            services.AddHttpContextAccessor();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();

            services.AddControllers(config =>
            {
                config.RespectBrowserAcceptHeader = true;
                config.ReturnHttpNotAcceptable = true;
                config.CacheProfiles.Add("120SecondsDuration", new CacheProfile { Duration = 120 });
            }).AddNewtonsoftJson();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.EnvironmentName == "development")
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();

            app.UseRequestLocalization();
            app.UseStaticFiles(new StaticFileOptions()
            {
                OnPrepareResponse =
                r =>
                {
                    var path = r.File.PhysicalPath;
                    if (!path.EndsWith(".css") && !path.EndsWith(".js") && !path.EndsWith(".png") &&
                        !path.EndsWith(".jpg") && !path.EndsWith(".ico") && !path.EndsWith(".jpeg"))
                        return;

                    var maxAge = new TimeSpan(7, 0, 0, 0);
                    r.Context.Response.Headers.Append("Cashe-Control", "max-age=" + maxAge.TotalSeconds.ToString("0"));
                }
            });
            app.UseCookiePolicy();
            app.UseCors("CorsPolicy");
            app.UseForwardedHeaders(new ForwardedHeadersOptions { ForwardedHeaders = ForwardedHeaders.All });

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "{controller=Inputs}/{id?}");
            });
        }
    }
}
